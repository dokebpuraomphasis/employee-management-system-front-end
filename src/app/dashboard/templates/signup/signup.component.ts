import { Component, OnInit } from '@angular/core';
import { SignupService } from '../../sevice/signup.service';
import { Router } from '@angular/router';
import { Employee } from '../../interface/employee';
import { HttpClient } from '@angular/common/http';
import { EmployeeService } from '../../sevice/employee.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  empName!: string;
  password!: string;
  employee!: {};

 
  selectedFile!: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message!: string;
  imageName: any;
  constructor(private service: SignupService, private router: Router, private httpClient: HttpClient,private employeeService :EmployeeService) { }

  ngOnInit(): void {
    console.log(this.empName);
  }


  gotoLogin() {
    this.router.navigate(['../login']);
  }


  //Gets called when the user selects an image
  public onFileChanged(event: any) {
    //Select File
    this.selectedFile = event.target.files[0];
    this.onUpload();
    this.getImage(this.selectedFile.name);
  }


  //Gets called when the user clicks on submit to upload the image
  onUpload() {
    console.log(this.selectedFile);


    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);

    //Make a call to the Spring Boot Application to save the image
    this.httpClient.post('http://localhost:9090/employee/upload', uploadImageData, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          this.message = 'Image uploaded successfully';
        } else {
          this.message = 'Image not uploaded successfully';
        }
      }
      );
  }

  getImage(imageName:string){
    this.employeeService.getImage(imageName).subscribe(
      res => {
        this.retrieveResonse = res;
        this.base64Data = this.retrieveResonse.picByte;
        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
      }
    );
  }

  onSubmit() {
   
    this.employee = {
      "empName": this.empName,
      "password": this.password,
      "name": this.selectedFile.name
    }

    console.log(this.selectedFile.name);
    
    this.service.createEmployee(this.employee).subscribe(data => {
      console.log(data);
    })
    this.gotoLogin();
  }
}
