import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../sevice/employee.service';
import { Employee } from '../../interface/employee';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss']
})
export class EmployeeDetailsComponent implements OnInit {

  id!: number;
  employee!: Employee;

  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  imageName:string='';
  
  constructor(private service: EmployeeService, private router:Router ,private route: ActivatedRoute,private httpClient:HttpClient) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    let imageName:string='';
    this.service.getEmployee(this.id).subscribe(data => {
      this.employee = data;
      imageName=this.employee.imageName;
      this.getImage(imageName);
    }, error => console.log(error));

    
  }

  getImage(imageName:string){
    this.service.getImage(imageName).subscribe(
      res => {
        this.retrieveResonse = res;
        this.base64Data = this.retrieveResonse.picByte;
        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
      }
    );
  }
  list(){
    this.router.navigate(['view']);
  }
}
